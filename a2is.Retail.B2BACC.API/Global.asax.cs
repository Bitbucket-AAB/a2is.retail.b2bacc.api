﻿using a2is.Framework.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace a2is.Retail.B2BACC.API
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var clientHttp = new HttpClient();
            clientHttp.Timeout = TimeSpan.FromMinutes(30);
            a2isAPIConfig.Configure();
        }
    }
}