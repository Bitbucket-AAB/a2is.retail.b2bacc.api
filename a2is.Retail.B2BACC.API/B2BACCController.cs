﻿using a2is.Framework.API;
using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using a2is.retail.B2B.Entities.ACC;
using a2is.retail.B2B.Entities.Endorsement;
using a2is.retail.B2B.Entities.General;
using a2is.Retail.B2B.ACC.GroupingAR.BL.Manager;
using a2is.Retail.B2B.Entities.ACC;
using a2is.Retail.B2B.Helper.B2BAPI;
using a2is.Retail.B2B.Helper.General;
using a2is.Retail.B2BACC.Cancellation.BL.Manager;
using a2is.Retail.B2BACC.Cancellation.BL.Repository;
using a2is.Retail.B2BACC.NewOrder.BL.Manager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace a2is.Retail.B2BACC.API
{
    public class B2BACCController : a2isAPIController
    {
        private static a2isLogHelper _log = new a2isLogHelper();
        private GeneralHelper B2BGeneralHelper = new GeneralHelper();



        #region NewOrder
        [HttpPost, Route("newOrder")]
        public IHttpActionResult newOrder(APIParam form)
        {
            ActionResult result = new ActionResult();
            ActionResult resultLog = new ActionResult();
            DateTime tmrStart;
            var clientHttp = new HttpClient();
            clientHttp.Timeout = TimeSpan.FromMinutes(30);

            #region logging
            //IsLoggingPerformance
            string isLoggingPerformance = ConfigurationManager.AppSettings["IsLoggingPerformance"];
            bool TmpResultLogging = false;
            tmrStart = DateTime.Now;
            #endregion
            try
            {
                _log.Debug("API New Order Start - " + form.RefferenceNo);

                #region request data from api/db
                string GetTaskDetailAPI = ConfigurationManager.AppSettings["GetTaskDetailAPI"];
                string B2BApplicationID_Order = ConfigurationManager.AppSettings["B2BApplicationID-Order"];

                #region cek reffno di core
                bool ReffNoExists = true;
                ReffNoExists = B2BGeneralHelper.CheckRefferenceNo(form.RefferenceNo, "NEWORDERACC");
                #region logging
                if (isLoggingPerformance == "1")
                {
                    B2BACC_Logs oB2BLogs = new B2BACC_Logs();
                    oB2BLogs.TrxType = "TMRCheckReffNo";
                    oB2BLogs.Entryusr = "b2bsa";
                    oB2BLogs.Entrydt = tmrStart;
                    oB2BLogs.OrderNo = "";
                    oB2BLogs.PolicyNo = "";
                    oB2BLogs.EndorsementNo = 0;
                    oB2BLogs.JsonResponse = form.RefferenceNo;
                    oB2BLogs.JsonRequest = "";
                    oB2BLogs.Updatedt = DateTime.Now;
                    oB2BLogs.Remarks = result.message;
                    TmpResultLogging = InsertB2BACC_Logs(oB2BLogs);
                }
                #endregion
                #endregion
                if (ReffNoExists == false)
                {
                    DateTime TMRGetMariaDB = DateTime.Now;
                    ActionResult OrderB2BAPI = B2BAPI.GetB2BAPIData("2", B2BApplicationID_Order, form.RefferenceNo, "b2bsa");
                    #region logging
                    if (isLoggingPerformance == "1")
                    {
                        B2BACC_Logs oB2BLogs = new B2BACC_Logs();
                        oB2BLogs.TrxType = "TMRGetMariaDB";
                        oB2BLogs.Entryusr = "b2bsa";
                        oB2BLogs.Entrydt = TMRGetMariaDB;
                        oB2BLogs.OrderNo = "";
                        oB2BLogs.PolicyNo = "";
                        oB2BLogs.EndorsementNo = 0;
                        oB2BLogs.JsonResponse = form.RefferenceNo;
                        oB2BLogs.JsonRequest = "";
                        oB2BLogs.Updatedt = DateTime.Now;
                        oB2BLogs.Remarks = result.message;
                        TmpResultLogging = InsertB2BACC_Logs(oB2BLogs);
                    }
                    #endregion
                    if (OrderB2BAPI.data == null)
                    {
                        _log.Info("Get MariaDB is null " + form.RefferenceNo + ":" + OrderB2BAPI.message);
                        result = InsertLogError("newOrder", form.RefferenceNo, "9999", "Get Data from MariaDB failed - " + OrderB2BAPI.message, "");
                        goto EndProcess;
                    }

                    _log.Debug("Get MariaDB Process - " + form.RefferenceNo);
                    #endregion
                    DateTime TMRSerializeASCII = DateTime.Now;
                    string ActionResultData = JsonConvert.SerializeObject(OrderB2BAPI.data);
                    _log.Debug("Get ActionResultData - " + ActionResultData);
                    JObject DataObject = JObject.Parse(OrderB2BAPI.data);
                    _log.Debug("Get DataObject - " + form.RefferenceNo);

                    #region logging
                    if (isLoggingPerformance == "1")
                    {
                        B2BACC_Logs oB2BLogs = new B2BACC_Logs();
                        oB2BLogs.TrxType = "TMRSerializeASCII";
                        oB2BLogs.Entryusr = "b2bsa";
                        oB2BLogs.Entrydt = TMRSerializeASCII;
                        oB2BLogs.OrderNo = "";
                        oB2BLogs.PolicyNo = "";
                        oB2BLogs.EndorsementNo = 0;
                        oB2BLogs.JsonResponse = form.RefferenceNo;
                        oB2BLogs.JsonRequest = "";
                        oB2BLogs.Updatedt = DateTime.Now;
                        oB2BLogs.Remarks = result.message;
                        TmpResultLogging = InsertB2BACC_Logs(oB2BLogs);
                    }
                    #endregion

                    if (DataObject != null)
                    {
                        string DictRequestData = Convert.ToString(DataObject);
                        _log.Debug("Get DictRequestData - " + form.RefferenceNo);
                        //string DictB2BRequestID = Convert.ToString(DictData["b2BRequestID"]);

                        if (DictRequestData != null)
                        {
                            string param = "newOrder";
                            DateTime TMRParsingASCII = DateTime.Now;
                            string requestDataToProcess = ParseRequestData(DictRequestData);
                            _log.Debug("Parserequestdata - " + form.RefferenceNo);
                            string FileName = ParseFIleName(DictRequestData);
                            _log.Debug("ParseFIleName - " + form.RefferenceNo);
                            result = ProsesDataASCII(requestDataToProcess, param);
                            _log.Debug("ProsesDataASCII - " + form.RefferenceNo);

                            #region logging
                            if (isLoggingPerformance == "1")
                            {
                                B2BACC_Logs oB2BLogs = new B2BACC_Logs();
                                oB2BLogs.TrxType = "TMRParsingASCII";
                                oB2BLogs.Entryusr = "b2bsa";
                                oB2BLogs.Entrydt = TMRParsingASCII;
                                oB2BLogs.OrderNo = "";
                                oB2BLogs.PolicyNo = "";
                                oB2BLogs.EndorsementNo = 0;
                                oB2BLogs.JsonResponse = form.RefferenceNo;
                                oB2BLogs.JsonRequest = "";
                                oB2BLogs.Updatedt = DateTime.Now;
                                oB2BLogs.Remarks = result.message;
                                TmpResultLogging = InsertB2BACC_Logs(oB2BLogs);
                            }
                            #endregion

                            if (result.isSuccess)
                            {
                                NewOrderManager iManager = new NewOrderManager();
                                result.data = iManager.ProcessTrxOrder(result.data);
                                _log.Debug("ProcessTrxOrder - " + form.RefferenceNo);
                                result.data = iManager.ReceiveNewOrder(result.data);
                                _log.Debug("ReceiveNewOrder - " + form.RefferenceNo);
                            }
                            else
                            {
                                result.data.ERROR_DESCRIPTION = "Error Parsing : " + result.message;
                            }

                            #region copy data
                            resultLog = ProsesDataASCII(requestDataToProcess, param);
                            resultLog.data.ERROR_CODE = result.data.ERROR_CODE;
                            resultLog.data.ERROR_DESCRIPTION = result.data.ERROR_DESCRIPTION;
                            resultLog.data.AABProductCode = result.data.AABProductCode;
                            resultLog.data.PARTNER_BRANCH_CODE = result.data.PARTNER_BRANCH_CODE;
                            resultLog.data.VehicleCode = result.data.VehicleCode;
                            resultLog.data.AABGeoArea = result.data.AABGeoArea;
                            #endregion

                            result = InsertLog(resultLog.data, param, form.RefferenceNo, FileName);
                            _log.Debug("InsertLog - " + form.RefferenceNo);
                        }

                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "Parsing Data Failed";
                        result = InsertLogError("newOrder", form.RefferenceNo, "9999", result.message, "");
                        _log.Info(MethodBase.GetCurrentMethod().Name + "Error : " + result.message);
                    }
                }
                else
                {
                    result.isSuccess = false;
                    result.message = "RefferenceNo already processed";
                    result = InsertLogError("newOrder", form.RefferenceNo, "9999", "ReffNo Already Processed", "");
                }

            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                result = InsertLogError("newOrder", form.RefferenceNo, "9999", result.message, "");
                _log.Error(MethodBase.GetCurrentMethod().Name + "Other Error : " + ex);
            }

            #region update Log
            if (isLoggingPerformance == "1")
            {
                B2BACC_Logs oB2BLogs = new B2BACC_Logs();
                oB2BLogs.TrxType = "newOrder";
                oB2BLogs.Entryusr = "b2bsa";
                oB2BLogs.Entrydt = tmrStart;
                oB2BLogs.OrderNo = "";
                oB2BLogs.PolicyNo = "";
                oB2BLogs.EndorsementNo = 0;
                oB2BLogs.JsonResponse = form.RefferenceNo;
                oB2BLogs.JsonRequest = "";
                oB2BLogs.Updatedt = DateTime.Now;
                oB2BLogs.Remarks = result.message;
                TmpResultLogging = InsertB2BACC_Logs(oB2BLogs);
            }
            #endregion
            EndProcess:
            _log.Debug("API New Order End");
            return Json(result);
        }

        #endregion
        #region CancelOrder
        [HttpPost, Route("cancelOrder")]
        public IHttpActionResult cancelOrder(APIParam form)
        {
            var clientHttp = new HttpClient();
            clientHttp.Timeout = TimeSpan.FromMinutes(30);
            ActionResult result = new ActionResult();
            try
            {
                _log.Debug("API Cancellation Start");

                #region request data from api
                string GetTaskDetailAPI = ConfigurationManager.AppSettings["GetTaskDetailAPI"];
                string B2BApplicationID = ConfigurationManager.AppSettings["B2BApplicationID-Cancel"];

                #region cek reffno di core
                bool ReffNoExists = true;
                ReffNoExists = B2BGeneralHelper.CheckRefferenceNo(form.RefferenceNo, "CANCELORDERACC");
                #endregion
                if (ReffNoExists == false)
                {
                    ActionResult OrderB2BAPI = B2BAPI.GetB2BAPIData("2", B2BApplicationID, form.RefferenceNo, "b2bsa");
                    if (OrderB2BAPI.data == null)
                    {
                        _log.Info("Get MariaDB is null " + form.RefferenceNo + ":" + OrderB2BAPI.message);
                        result = InsertLogError("cancelOrder", form.RefferenceNo, "9999", "Get Data from MariaDB failed - " + OrderB2BAPI.message, "");
                        goto EndProcess;
                    }
                    #endregion
                    JObject DataObject = JObject.Parse(OrderB2BAPI.data);
                    if (DataObject != null)
                    {
                        string DictRequestData = Convert.ToString(DataObject);

                        if (DictRequestData != null)
                        {
                            string param = "cancelOrder";
                            string requestDataToProcess = ParseRequestData(DictRequestData);
                            string FileName = ParseFIleName(DictRequestData);
                            result = ProsesDataASCII(requestDataToProcess, param);

                            if (result.isSuccess)
                            {
                                result.data.Order_Type = "4";
                                CancellationManager iManager = new CancellationManager();
                                result = iManager.ProcessCancellation(result.data);

                            }
                            else
                            {
                                result.data.ErrorDescription = "Error Parsing : " + result.message;
                            }


                            result = InsertLog(result.data, param, form.RefferenceNo, FileName);
                        }

                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "Parsing Data Failed";
                        result = InsertLogError("cancelOrder", form.RefferenceNo, "9999", result.message, "");
                        _log.Info(MethodBase.GetCurrentMethod().Name + "Error : " + result.message);
                    }

                }
                else
                {
                    result = InsertLogError("cancelOrder", form.RefferenceNo, "9999", "ReffNo Already Processed", "");
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                result = InsertLogError("cancelOrder", form.RefferenceNo, "9999", result.message, "");
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + result.message);
            }
            EndProcess:
            _log.Debug("API Cancellation End");
            return Json(result);
        }
        #endregion
        #region GroupingAR
        [HttpPost, Route("groupingAR")]
        public IHttpActionResult groupingAR(APIParam form)
        {
            var clientHttp = new HttpClient();
            clientHttp.Timeout = TimeSpan.FromMinutes(30);
            ActionResult result = new ActionResult();
            try
            {
                _log.Debug("API groupingAR Start");

                #region request data from DB Maria
                string GetTaskDetailAPI = ConfigurationManager.AppSettings["GetTaskDetailAPI"];
                string B2BApplicationID_Group = ConfigurationManager.AppSettings["B2BApplicationID-Group"];

                #region cek reffno di core
                bool ReffNoExists = true;
                ReffNoExists = B2BGeneralHelper.CheckRefferenceNo(form.RefferenceNo, "GROUPINGORDERACC");
                #endregion
                if (ReffNoExists == false)
                {
                    ActionResult OrderB2BAPI = B2BAPI.GetB2BAPIData("2", B2BApplicationID_Group, form.RefferenceNo, "b2bsa");
                    if (OrderB2BAPI.data == null)
                    {
                        _log.Info("Get MariaDB is null " + form.RefferenceNo + ":" + OrderB2BAPI.message);
                        result = InsertLogError("groupingAR", form.RefferenceNo, "9999", "Get Data from MariaDB failed - " + OrderB2BAPI.message, "");
                        goto EndProcess;
                    }
                    #endregion

                    JObject DataObject = JObject.Parse(OrderB2BAPI.data);
                    if (DataObject != null)
                    {
                        string DictRequestData = Convert.ToString(DataObject);
                        //string DictB2BRequestID = Convert.ToString(DictData["b2BRequestID"]);

                        if (DictRequestData != null)
                        {
                            string param = "groupingAR";
                            string requestDataToProcess = ParseRequestData(DictRequestData);
                            string FileName = ParseFIleName(DictRequestData);
                            string[] arrDictRequestData = requestDataToProcess.Split(new string[] { "\n" }, StringSplitOptions.None);
                            result.data = new List<TrxGroupingAR>();

                            foreach (var item in arrDictRequestData)
                            {
                                ActionResult tempResult = ProsesDataASCII(item, param);

                                if (!tempResult.isSuccess)
                                {
                                    result.isSuccess = false;
                                    tempResult.data.ERROR_DESCRIPTION = "Error Parsing : " + tempResult.message;
                                    result.data.Add(tempResult.data);

                                    break;
                                }

                                result.data.Add(tempResult.data);
                            }

                            if (result.isSuccess)
                            {
                                GroupingARManager iManager = new GroupingARManager();
                                result = iManager.GetGroupingAR(result.data);
                            }


                            result = InsertLog(result.data, param, form.RefferenceNo, FileName);
                        }

                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "Parsing Data Failed";
                        _log.Info(MethodBase.GetCurrentMethod().Name + "Error : " + result.message);
                        result = InsertLogError("groupingAR", form.RefferenceNo, "9999", result.message, "");
                    }
                }
                else
                {
                    result = InsertLogError("groupingAR", form.RefferenceNo, "9999", "ReffNo Already Processed", "");
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                result = InsertLogError("groupingAR", form.RefferenceNo, "9999", result.message, "");
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }
            EndProcess:
            _log.Debug("API groupingAR End");
            return Json(result);
        }

        #endregion

        #region Others
        private IRestResponse APIGetCaller(string urlhost, JObject parameter, string token = null)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var client = new RestClient(urlhost);

            var request = new RestRequest(Method.POST);

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            request.AddHeader("accept", "application/json");

            request.AddHeader("content-type", "application/json");

            request.AddHeader("Authorization", "Bearer " + token);

            request.AddParameter("application/json", parameter, ParameterType.RequestBody);

            return client.Execute(request);
        }

        public ActionResult ProsesDataASCII(string asciiData, string param)
        {
            ActionResult result = new ActionResult();

            try
            {
                string query = "";
                switch (param)
                {
                    case "newOrder": query = @"; SELECT name, start, length FROM acc_new_order_detail"; result.data = new TrxNewOrder(); break;
                    case "cancelOrder": query = @"; SELECT name, start, length FROM acc_cancel_order_detail"; result.data = new CancelOrder(); break;
                    case "groupingAR": query = @"; SELECT name, start, length FROM acc_grouping_ar_detail"; result.data = new TrxGroupingAR(); break;
                    default: break;
                }

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    //get detail data
                    var detailData = db.Fetch<dynamic>(query);

                    foreach (var item in detailData)
                    {
                        if (Convert.ToInt32(item.start + item.length) <= asciiData.Length) // validasi agar tidak melebihi length dari ascii data
                        {
                            PropertyInfo prop = result.data.GetType().GetProperty(item.name.ToString(), BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                            if (null != prop && prop.CanWrite)
                            {
                                if (prop.PropertyType == typeof(DateTime))
                                {
                                    string tempVar = asciiData.Substring(Convert.ToInt32(item.start), Convert.ToInt32(item.length));
                                    if (tempVar.Trim() == "")
                                    {
                                        tempVar = "01/01/1900";
                                    }

                                    DateTime newDate = new DateTime();
                                    if (DateTime.TryParseExact(tempVar, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate))
                                    {
                                        prop.SetValue(result.data, newDate, null);
                                    }
                                    else
                                    {
                                        result.isSuccess = false;
                                        result.message = "Error while parsing data tipe datetime : " + tempVar;
                                        _log.Error(MethodBase.GetCurrentMethod().Name + "Error while parsing datetime : " + tempVar);

                                        return result;
                                    }
                                }
                                else if (prop.PropertyType == typeof(int))
                                {
                                    string tempVar = asciiData.Substring(Convert.ToInt32(item.start), Convert.ToInt32(item.length));
                                    if (tempVar.IndexOf(".") != -1)
                                    {
                                        tempVar = tempVar.Substring(0, tempVar.IndexOf("."));
                                    }
                                    prop.SetValue(result.data, Convert.ToInt32(tempVar), null);
                                }
                                else if (prop.PropertyType == typeof(decimal))
                                {
                                    string tempVar = asciiData.Substring(Convert.ToInt32(item.start), Convert.ToInt32(item.length));
                                    prop.SetValue(result.data, decimal.Parse(tempVar, CultureInfo.InvariantCulture), null);
                                }
                                else
                                {
                                    prop.SetValue(result.data, asciiData.Substring(Convert.ToInt32(item.start), Convert.ToInt32(item.length)), null);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }

            return result;
        }

        public ActionResult InsertLog(dynamic data, string param, string reffNo, string FileName = "")
        {
            ActionResult result = new ActionResult();

            string UserID = ConfigurationManager.AppSettings["EntryUserID"];

            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {
                    #region query insert partner order log
                    string qInsertPartnerOrderLog = @";
                                        DECLARE @@Contract_Number VARCHAR(50) = @0
                                                ,@@Product_Code VARCHAR(50) = @1
                                                ,@@Partner_Branch_Code VARCHAR(50) = @2
                                                ,@@Engine_Number VARCHAR(50) = @3
                                                ,@@Chassis_Number VARCHAR(50) = @4
                                                ,@@Period_From VARCHAR(50) = @5
                                                ,@@Period_To VARCHAR(50) = @6
                                                ,@@Client_Number VARCHAR(50) = @7
                                                ,@@Vehicle_Code VARCHAR(50) = @8
                                                ,@@Geographical_Area VARCHAR(50) = @9
                                                ,@@Manufacturing_Year VARCHAR(50) = @10
                                                ,@@Order_Type VARCHAR(50) = @11
                                                ,@@Policy_No VARCHAR(50) = @12
                                                ,@@Endorsement_No VARCHAR(50) = @13
                                                ,@@Effective_Date VARCHAR(50) = @14
                                                ,@@Name VARCHAR(50) = @15
                                                ,@@Error_Code VARCHAR(255) = @16
                                                ,@@Error_Description VARCHAR(255) = @17
                                                ,@@entryusr VARCHAR(50) = @18
                                                ,@@entrydt VARCHAR(50) = @19
                                                ,@@updateusr VARCHAR(50) = @20
                                                ,@@updatedt VARCHAR(50) = @21
                                                ,@@Partner_Id VARCHAR(50) = @22
                                                ,@@Numeric_Error_Code_1 VARCHAR(50) = @23
                                                ,@@Numeric_Error_Code_2 VARCHAR(50) = @24
                                                ,@@Object_No VARCHAR(50) = @25
                                                ,@@Endorsement_Map VARCHAR(50) = @26
                                                ,@@guid VARCHAR(50) = @27
                                                ,@@Error_Description2 VARCHAR(255) = @28
                                                ,@@Flag_Value_Chain VARCHAR(50) = @29
                                                ,@@GROUP_NO VARCHAR(50) = @30,@@Send_Status int = @31
                                        INSERT INTO [dbo].[Partner_Orders_Log]
                                                   ([Contract_Number]
                                                   ,[Product_Code]
                                                   ,[Partner_Branch_Code]
                                                   ,[Engine_Number]
                                                   ,[Chassis_Number]
                                                   ,[Period_From]
                                                   ,[Period_To]
                                                   ,[Client_Number]
                                                   ,[Vehicle_Code]
                                                   ,[Geographical_Area]
                                                   ,[Manufacturing_Year]
                                                   ,[Order_Type]
                                                   ,[Policy_No]
                                                   ,[Endorsement_No]
                                                   ,[Effective_Date]
                                                   ,[Name]
                                                   ,[Error_Code]
                                                   ,[Error_Description]
                                                   ,[entryusr]
                                                   ,[entrydt]
                                                   ,[updateusr]
                                                   ,[updatedt]
                                                   ,[Partner_Id]
                                                   ,[Numeric_Error_Code_1]
                                                   ,[Numeric_Error_Code_2]
                                                   ,[Object_No]
                                                   ,[Endorsement_Map]
                                                   ,[guid]
                                                   ,[Error_Description2]
                                                   ,[Flag_Value_Chain]
                                                   ,[GROUP_NO],Send_Status)
                                             VALUES
                                                   (@@Contract_Number
                                                   ,@@Product_Code
                                                   ,@@Partner_Branch_Code
                                                   ,@@Engine_Number
                                                   ,@@Chassis_Number
                                                   ,@@Period_From
                                                   ,@@Period_To
                                                   ,@@Client_Number
                                                   ,@@Vehicle_Code
                                                   ,@@Geographical_Area
                                                   ,@@Manufacturing_Year
                                                   ,@@Order_Type
                                                   ,@@Policy_No
                                                   ,@@Endorsement_No
                                                   ,@@Effective_Date
                                                   ,@@Name
                                                   ,@@Error_Code
                                                   ,@@Error_Description
                                                   ,@@entryusr
                                                   ,@@entrydt
                                                   ,@@updateusr
                                                   ,@@updatedt
                                                   ,@@Partner_Id
                                                   ,@@Numeric_Error_Code_1
                                                   ,@@Numeric_Error_Code_2
                                                   ,@@Object_No
                                                   ,@@Endorsement_Map
                                                   ,@@guid
                                                   ,@@Error_Description2
                                                   ,@@Flag_Value_Chain
                                                   ,@@GROUP_NO,@@Send_Status)
                                        ";
                    #endregion

                    if (param == "newOrder")
                    {
                        #region insert log into acc_new_order
                        #region query
                        string query = @";
                            DECLARE @@kode_pt VARCHAR(50) = @0
                                ,@@nama_pt VARCHAR(50) = @1
                                ,@@kode_jf VARCHAR(50) = @2
                                ,@@nama_jf VARCHAR(50) = @3
                                ,@@kode_cabang VARCHAR(50) = @4
                                ,@@no_kontrak VARCHAR(50) = @5
                                ,@@cd_customer VARCHAR(50) = @6
                                ,@@tgl_valid VARCHAR(50) = @7
                                ,@@flag_per_co VARCHAR(50) = @8
                                ,@@title_customer VARCHAR(50) = @9
                                ,@@nama VARCHAR(50) = @10
                                ,@@alamat_rmh VARCHAR(50) = @11
                                ,@@kota_rmh VARCHAR(50) = @12
                                ,@@kodepos_rmh VARCHAR(50) = @13
                                ,@@phone_resident VARCHAR(50) = @14
                                ,@@alamat_surat VARCHAR(50) = @15
                                ,@@kota_surat VARCHAR(50) = @16
                                ,@@kodepos_surat VARCHAR(50) = @17
                                ,@@title_kantor VARCHAR(50) = @18
                                ,@@nama_kantor VARCHAR(50) = @19
                                ,@@alamat_kantor VARCHAR(50) = @20
                                ,@@kota_kantor VARCHAR(50) = @21
                                ,@@kodepos_kantor VARCHAR(50) = @22
                                ,@@telepon_kantor VARCHAR(50) = @23
                                ,@@cd_brand VARCHAR(50) = @24
                                ,@@cd_type VARCHAR(50) = @25
                                ,@@cd_model VARCHAR(50) = @26
                                ,@@desc_brand VARCHAR(50) = @27
                                ,@@desc_type VARCHAR(50) = @28
                                ,@@desc_model VARCHAR(50) = @29
                                ,@@desc_kind VARCHAR(50) = @30
                                ,@@cd_color VARCHAR(50) = @31
                                ,@@desc_color VARCHAR(50) = @32
                                ,@@year_of_mfg VARCHAR(50) = @33
                                ,@@new_used VARCHAR(50) = @34
                                ,@@type_used VARCHAR(50) = @35
                                ,@@no_car_police VARCHAR(50) = @36
                                ,@@no_chasis VARCHAR(50) = @37
                                ,@@no_engine VARCHAR(50) = @38
                                ,@@cd_dealer_br VARCHAR(50) = @39
                                ,@@desc_dealer_br VARCHAR(50) = @40
                                ,@@no_ar VARCHAR(50) = @41
                                ,@@no_tlo VARCHAR(50) = @42
                                ,@@from_due_date VARCHAR(50) = @43
                                ,@@to_due_date VARCHAR(50) = @44
                                ,@@amt_insu_cover VARCHAR(50) = @45
                                ,@@selling_rate VARCHAR(50) = @46
                                ,@@rate_srcc VARCHAR(50) = @47
                                ,@@premium VARCHAR(50) = @48
                                ,@@premium_srcc VARCHAR(50) = @49
                                ,@@amt_thirdparty VARCHAR(50) = @50
                                ,@@amt_admin_thirdparty VARCHAR(50) = @51
                                ,@@amt_onrisk VARCHAR(50) = @52
                                ,@@dt_appl VARCHAR(50) = @53
                                ,@@tgl_lahir VARCHAR(50) = @54
                                ,@@cd_sex VARCHAR(50) = @55
                                ,@@cd_marital_status VARCHAR(50) = @56
                                ,@@cd_education VARCHAR(50) = @57
                                ,@@desc_education VARCHAR(50) = @58
                                ,@@cd_job VARCHAR(50) = @59
                                ,@@desc_job VARCHAR(50) = @60
                                ,@@phone_mobile VARCHAR(50) = @61
                                ,@@admin_insu VARCHAR(50) = @62
                                ,@@net_rate VARCHAR(50) = @63
                                ,@@net_premi VARCHAR(50) = @64
                                ,@@name_salesman VARCHAR(50) = @65
                                ,@@cd_salesman VARCHAR(50) = @66
                                ,@@no_rekening VARCHAR(50) = @67
                                ,@@nama_bank VARCHAR(50) = @68
                                ,@@cd_bank_br VARCHAR(50) = @69
                                ,@@cd_bank_ho VARCHAR(50) = @70
                                ,@@flag_gap VARCHAR(50) = @71
                                ,@@gross_rate VARCHAR(50) = @72
                                ,@@gross_premi VARCHAR(50) = @73
                                ,@@cd_vehicle_category VARCHAR(50) = @74
                                ,@@cd_product VARCHAR(50) = @75
                                ,@@cd_insu_ho VARCHAR(50) = @76
                                ,@@flag_value_chain VARCHAR(50) = @77
                                ,@@rate_flood VARCHAR(50) = @78
                                ,@@premium_flood VARCHAR(50) = @79
                                ,@@rate_earthquake VARCHAR(50) = @80
                                ,@@premium_earthquake VARCHAR(50) = @81
                                ,@@rate_terrorism VARCHAR(50) = @82
                                ,@@premium_terrorism VARCHAR(50) = @83
                                ,@@rate_srccts VARCHAR(50) = @84
                                ,@@premium_srccts VARCHAR(50) = @85
                                ,@@suminsured_padriver VARCHAR(50) = @86
                                ,@@rate_padriver VARCHAR(50) = @87
                                ,@@premium_padriver VARCHAR(50) = @88
                                ,@@suminsured_papassanger VARCHAR(50) = @89
                                ,@@number_of_seats VARCHAR(50) = @90
                                ,@@rate_papassanger VARCHAR(50) = @91
                                ,@@premium_papassanger VARCHAR(50) = @92
                                ,@@suminsured_tpl VARCHAR(50) = @93
                                ,@@rate_tpl VARCHAR(50) = @94
                                ,@@premium_tpl VARCHAR(50) = @95
                                ,@@suminsured_pll VARCHAR(50) = @96
                                ,@@rate_pll VARCHAR(50) = @97
                                ,@@premium_pll VARCHAR(50) = @98
                                ,@@no_tpl VARCHAR(50) = @99
                                ,@@cd_wilayah VARCHAR(50) = @100
                                ,@@sales_no VARCHAR(50) = @101
                                ,@@cd_dealer_br_ai VARCHAR(50) = @102
                                ,@@flag_digi VARCHAR(50) = @103
                                ,@@order_type VARCHAR(50) = @104
                                ,@@send_status VARCHAR(50) = @105
                                ,@@send_date VARCHAR(50) = @106
                                ,@@error_code VARCHAR(50) = @107
                                ,@@error_description VARCHAR(250) = @108
                                ,@@reff_no VARCHAR(50) = @109
                                ,@@file_name VARCHAR(50) = @110
                                ,@@create_by VARCHAR(50) = @111
                                ,@@create_date DATETIME = @112
                                ,@@cd_kind VARCHAR(50) = @113

                            INSERT INTO [dbo].[acc_new_order]
                                       ([kode_pt]
                                       ,[nama_pt]
                                       ,[kode_jf]
                                       ,[nama_jf]
                                       ,[kode_cabang]
                                       ,[no_kontrak]
                                       ,[cd_customer]
                                       ,[tgl_valid]
                                       ,[flag_per_co]
                                       ,[title_customer]
                                       ,[nama]
                                       ,[alamat_rmh]
                                       ,[kota_rmh]
                                       ,[kodepos_rmh]
                                       ,[phone_resident]
                                       ,[alamat_surat]
                                       ,[kota_surat]
                                       ,[kodepos_surat]
                                       ,[title_kantor]
                                       ,[nama_kantor]
                                       ,[alamat_kantor]
                                       ,[kota_kantor]
                                       ,[kodepos_kantor]
                                       ,[telepon_kantor]
                                       ,[cd_brand]
                                       ,[cd_type]
                                       ,[cd_model]
                                       ,[desc_brand]
                                       ,[desc_type]
                                       ,[desc_model]
                                       ,[desc_kind]
                                       ,[cd_color]
                                       ,[desc_color]
                                       ,[year_of_mfg]
                                       ,[new_used]
                                       ,[type_used]
                                       ,[no_car_police]
                                       ,[no_chasis]
                                       ,[no_engine]
                                       ,[cd_dealer_br]
                                       ,[desc_dealer_br]
                                       ,[no_ar]
                                       ,[no_tlo]
                                       ,[from_due_date]
                                       ,[to_due_date]
                                       ,[amt_insu_cover]
                                       ,[selling_rate]
                                       ,[rate_srcc]
                                       ,[premium]
                                       ,[premium_srcc]
                                       ,[amt_thirdparty]
                                       ,[amt_admin_thirdparty]
                                       ,[amt_onrisk]
                                       ,[dt_appl]
                                       ,[tgl_lahir]
                                       ,[cd_sex]
                                       ,[cd_marital_status]
                                       ,[cd_education]
                                       ,[desc_education]
                                       ,[cd_job]
                                       ,[desc_job]
                                       ,[phone_mobile]
                                       ,[admin_insu]
                                       ,[net_rate]
                                       ,[net_premi]
                                       ,[name_salesman]
                                       ,[cd_salesman]
                                       ,[no_rekening]
                                       ,[nama_bank]
                                       ,[cd_bank_br]
                                       ,[cd_bank_ho]
                                       ,[flag_gap]
                                       ,[gross_rate]
                                       ,[gross_premi]
                                       ,[cd_vehicle_category]
                                       ,[cd_product]
                                       ,[cd_insu_ho]
                                       ,[flag_value_chain]
                                       ,[rate_flood]
                                       ,[premium_flood]
                                       ,[rate_earthquake]
                                       ,[premium_earthquake]
                                       ,[rate_terrorism]
                                       ,[premium_terrorism]
                                       ,[rate_srccts]
                                       ,[premium_srccts]
                                       ,[suminsured_padriver]
                                       ,[rate_padriver]
                                       ,[premium_padriver]
                                       ,[suminsured_papassanger]
                                       ,[number_of_seats]
                                       ,[rate_papassanger]
                                       ,[premium_papassanger]
                                       ,[suminsured_tpl]
                                       ,[rate_tpl]
                                       ,[premium_tpl]
                                       ,[suminsured_pll]
                                       ,[rate_pll]
                                       ,[premium_pll]
                                       ,[no_tpl]
                                       ,[cd_wilayah]
                                       ,[sales_no]
                                       ,[cd_dealer_br_ai]
                                       ,[flag_digi]
                                       ,[order_type]
                                       ,[send_status]
                                       ,[send_date]
                                       ,[error_code]
                                       ,[error_description]
                                       ,[reff_no]
                                       ,[file_name]
                                       ,[create_by]
                                       ,[create_date]
                                       ,[cd_kind])
                                 VALUES
                                      (@@kode_pt
                                       ,@@nama_pt
                                       ,@@kode_jf
                                       ,@@nama_jf
                                       ,@@kode_cabang
                                       ,@@no_kontrak
                                       ,@@cd_customer
                                       ,@@tgl_valid
                                       ,@@flag_per_co
                                       ,@@title_customer
                                       ,@@nama
                                       ,@@alamat_rmh
                                       ,@@kota_rmh
                                       ,@@kodepos_rmh
                                       ,@@phone_resident
                                       ,@@alamat_surat
                                       ,@@kota_surat
                                       ,@@kodepos_surat
                                       ,@@title_kantor
                                       ,@@nama_kantor
                                       ,@@alamat_kantor
                                       ,@@kota_kantor
                                       ,@@kodepos_kantor
                                       ,@@telepon_kantor
                                       ,@@cd_brand
                                       ,@@cd_type
                                       ,@@cd_model
                                       ,@@desc_brand
                                       ,@@desc_type
                                       ,@@desc_model
                                       ,@@desc_kind
                                       ,@@cd_color
                                       ,@@desc_color
                                       ,@@year_of_mfg
                                       ,@@new_used
                                       ,@@type_used
                                       ,@@no_car_police
                                       ,@@no_chasis
                                       ,@@no_engine
                                       ,@@cd_dealer_br
                                       ,@@desc_dealer_br
                                       ,@@no_ar
                                       ,@@no_tlo
                                       ,@@from_due_date
                                       ,@@to_due_date
                                       ,@@amt_insu_cover
                                       ,@@selling_rate
                                       ,@@rate_srcc
                                       ,@@premium
                                       ,@@premium_srcc
                                       ,@@amt_thirdparty
                                       ,@@amt_admin_thirdparty
                                       ,@@amt_onrisk
                                       ,@@dt_appl
                                       ,@@tgl_lahir
                                       ,@@cd_sex
                                       ,@@cd_marital_status
                                       ,@@cd_education
                                       ,@@desc_education
                                       ,@@cd_job
                                       ,@@desc_job
                                       ,@@phone_mobile
                                       ,@@admin_insu
                                       ,@@net_rate
                                       ,@@net_premi
                                       ,@@name_salesman
                                       ,@@cd_salesman
                                       ,@@no_rekening
                                       ,@@nama_bank
                                       ,@@cd_bank_br
                                       ,@@cd_bank_ho
                                       ,@@flag_gap
                                       ,@@gross_rate
                                       ,@@gross_premi
                                       ,@@cd_vehicle_category
                                       ,@@cd_product
                                       ,@@cd_insu_ho
                                       ,@@flag_value_chain
                                       ,@@rate_flood
                                       ,@@premium_flood
                                       ,@@rate_earthquake
                                       ,@@premium_earthquake
                                       ,@@rate_terrorism
                                       ,@@premium_terrorism
                                       ,@@rate_srccts
                                       ,@@premium_srccts
                                       ,@@suminsured_padriver
                                       ,@@rate_padriver
                                       ,@@premium_padriver
                                       ,@@suminsured_papassanger
                                       ,@@number_of_seats
                                       ,@@rate_papassanger
                                       ,@@premium_papassanger
                                       ,@@suminsured_tpl
                                       ,@@rate_tpl
                                       ,@@premium_tpl
                                       ,@@suminsured_pll
                                       ,@@rate_pll
                                       ,@@premium_pll
                                       ,@@no_tpl
                                       ,@@cd_wilayah
                                       ,@@sales_no
                                       ,@@cd_dealer_br_ai
                                       ,@@flag_digi
                                       ,@@order_type
                                       ,@@send_status
                                       ,@@send_date
                                       ,@@error_code
                                       ,@@error_description
                                       ,@@reff_no
                                       ,@@file_name
                                       ,@@create_by
                                       ,@@create_date
                                       ,@@cd_kind)";
                        #endregion
                        db.Execute(query, data.KODE_PT
                                       , data.NAMA_PT
                                       , data.KODE_JF
                                       , data.NAMA_JF
                                       , data.KODE_CABANG
                                       , data.NO_KONTRAK
                                       , data.CD_CUSTOMER
                                       , data.TGL_VALID.ToString("dd/MM/yyyy")
                                       , data.FLAG_PER_CO
                                       , data.TITLE_CUSTOMER
                                       , data.NAMA
                                       , data.ALAMAT_RMH
                                       , data.KOTA_RMH
                                       , data.KODEPOS_RMH
                                       , data.PHONE_RESIDENT
                                       , data.ALAMAT_SURAT
                                       , data.KOTA_SURAT
                                       , data.KODEPOS_SURAT
                                       , data.TITLE_KANTOR
                                       , data.NAMA_KANTOR
                                       , data.ALAMAT_KANTOR
                                       , data.KOTA_KANTOR
                                       , data.KODEPOS_KANTOR
                                       , data.TELEPON_KANTOR
                                       , data.CD_BRAND
                                       , data.CD_TYPE
                                       , data.CD_MODEL
                                       , data.DESC_BRAND
                                       , data.DESC_TYPE
                                       , data.DESC_MODEL
                                       , data.DESC_KIND
                                       , data.CD_COLOR
                                       , data.DESC_COLOR
                                       , data.YEAR_OF_MFG
                                       , data.NEW_USED
                                       , data.TYPE_USED
                                       , data.NO_CAR_POLICE
                                       , data.NO_CHASIS
                                       , data.NO_ENGINE
                                       , data.CD_DEALER_BR
                                       , data.DESC_DEALER_BR
                                       , data.NO_AR
                                       , data.NO_TLO
                                       , data.FROM_DUE_DATE.ToString("dd/MM/yyyy")
                                       , data.TO_DUE_DATE.ToString("dd/MM/yyyy")
                                       , data.AMT_INSU_COVER
                                       , data.SELLING_RATE
                                       , data.RATE_SRCC
                                       , data.PREMIUM
                                       , data.PREMIUM_SRCC
                                       , data.AMT_THIRDPARTY
                                       , data.AMT_ADMIN_THIRDPARTY
                                       , data.AMT_ONRISK
                                       , data.DT_APPL.ToString("dd/MM/yyyy")
                                       , data.TGL_LAHIR.ToString("dd/MM/yyyy")
                                       , data.CD_SEX
                                       , data.CD_MARITAL_STATUS
                                       , data.CD_EDUCATION
                                       , data.DESC_EDUCATION
                                       , data.CD_JOB
                                       , data.DESC_JOB
                                       , data.PHONE_MOBILE
                                       , data.ADMIN_INSU
                                       , data.NET_RATE
                                       , data.NET_PREMI
                                       , data.NAME_SALESMAN
                                       , data.CD_SALESMAN
                                       , data.NO_REKENING
                                       , data.NAMA_BANK
                                       , data.CD_BANK_BR
                                       , data.CD_BANK_HO
                                       , data.FLAG_GAP
                                       , data.GROSS_RATE
                                       , data.GROSS_PREMI
                                       , data.CD_VEHICLE_CATEGORY
                                       , data.CD_PRODUCT
                                       , data.CD_INSU_HO
                                       , data.FLAG_VALUE_CHAIN
                                       , data.RATE_FLOOD
                                       , data.PREMIUM_FLOOD
                                       , data.RATE_EARTHQUAKE
                                       , data.PREMIUM_EARTHQUAKE
                                       , data.RATE_TERRORISM
                                       , data.PREMIUM_TERRORISM
                                       , data.RATE_SRCCTS
                                       , data.PREMIUM_SRCCTS
                                       , data.SUMINSURED_PADRIVER
                                       , data.RATE_PADRIVER
                                       , data.PREMIUM_PADRIVER
                                       , data.SUMINSURED_PAPASSANGER
                                       , data.NUMBER_OF_SEATS
                                       , data.RATE_PAPASSANGER
                                       , data.PREMIUM_PAPASSANGER
                                       , data.SUMINSURED_TPL
                                       , data.RATE_TPL
                                       , data.PREMIUM_TPL
                                       , data.SUMINSURED_PLL
                                       , data.RATE_PLL
                                       , data.PREMIUM_PLL
                                       , data.NO_TPL
                                       , data.CD_WILAYAH
                                       , data.SALES_NO
                                       , data.CD_DEALER_BR_AI
                                       , data.FLAG_DIGI
                                       , "1"
                                       , "0"
                                       , ""
                                       , data.ERROR_CODE
                                       , data.ERROR_DESCRIPTION
                                       , reffNo
                                       , FileName
                                       , UserID
                                       , DateTime.Now
                                       , data.CD_KIND);
                        #endregion

                        #region insert log into partner_orders_log

                        db.Execute(qInsertPartnerOrderLog,
                                data.NO_KONTRAK //@@Contract_Number
                                , data.AABProductCode //@@Product_Code
                                , data.PARTNER_BRANCH_CODE //@@Partner_Branch_Code
                                , data.NO_ENGINE //@@Engine_Number
                                , data.NO_CHASIS //@@Chassis_Number
                                , data.FROM_DUE_DATE.ToString("MM/dd/yyyy") //@@Period_From
                                , data.TO_DUE_DATE.ToString("MM/dd/yyyy") //@@Period_To
                                , data.CD_CUSTOMER //@@Client_Number
                                , data.VehicleCode //@@Vehicle_Code
                                , data.AABGeoArea //@@Geographical_Area
                                , data.YEAR_OF_MFG //@@Manufacturing_Year
                                , "1" //@@Order_Type
                                , data.PolicyNo //@@Policy_No
                                , null//@@Endorsement_No
                                , null //@@Effective_Date
                                , null //@@Name
                                , data.ERROR_CODE //@@Error_Code
                                , data.ERROR_DESCRIPTION //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , null //@@updateusr
                                , null //@@updatedt
                                , data.PartnerID //@@Partner_Id
                                , null //@@Numeric_Error_Code_1
                                , null //@@Numeric_Error_Code_2
                                , null //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO,
                                , 0
                                );

                        #endregion
                    }
                    else if (param == "cancelOrder")
                    {
                        #region insert log into acc_cancel_order
                        #region query
                        string query = @";
                            DECLARE @@tanggal_cancel VARCHAR(50) = @0, 
                                    @@nomor_polis VARCHAR(50) = @1,
                                    @@kode_pt VARCHAR(50) = @2,
                                    @@total_data VARCHAR(50) = @3,
                                    @@order_type VARCHAR(50) = @4,
                                    @@send_status VARCHAR(50) = @5,
                                    @@send_date VARCHAR(50) = @6,
                                    @@error_code VARCHAR(50) = @7,
                                    @@error_description VARCHAR(250) = @8,
                                    @@reff_no VARCHAR(50) = @9,
                                    @@file_name VARCHAR(50) = @10,
                                    @@create_by VARCHAR(50) = @11,
                                    @@create_date DATETIME = @12

                            INSERT INTO [dbo].[acc_cancel_order]
                                        ([tanggal_cancel]
                                        ,[nomor_polis]
                                        ,[kode_pt]
                                        ,[total_data]
                                        ,[order_type]
                                        ,[send_status]
                                        ,[send_date]
                                        ,[error_code]
                                        ,[error_description]
                                        ,[reff_no]
                                        ,[file_name]
                                        ,[create_by]
                                        ,[create_date])
                                VALUES 
                                        (@@tanggal_cancel
                                        ,@@nomor_polis
                                        ,@@kode_pt
                                        ,@@total_data
                                        ,@@order_type
                                        ,@@send_status
                                        ,@@send_date
                                        ,@@error_code
                                        ,@@error_description
                                        ,@@reff_no
                                        ,@@file_name
                                        ,@@create_by
                                        ,@@create_date)";
                        #endregion

                        db.Execute(query, data.Tanggal_Cancel, data.Nomor_Polis.Trim(), data.Kode_PT, data.Total_Data, data.Order_Type, "0", "", data.ErrorCode, data.ErrorDescription, reffNo, FileName, UserID, DateTime.Now);
                        #endregion
                        #region insert into partner_order_log
                        db.Execute(qInsertPartnerOrderLog,
                                data.ContractNumber == null ? "" : data.ContractNumber //@@Contract_Number
                                , data.ProductCode //@@Product_Code
                                , null //@@Partner_Branch_Code
                                , null //@@Engine_Number
                                , null //@@Chassis_Number
                                , null //@@Period_From
                                , null //@@Period_To
                                , null //@@Client_Number
                                , null //@@Vehicle_Code
                                , null //@@Geographical_Area
                                , null //@@Manufacturing_Year
                                , data.Order_Type //@@Order_Type
                                , data.Nomor_Polis.Trim() //@@Policy_No
                                , null//@@Endorsement_No
                                , data.EffectiveDate == DateTime.MinValue ? null : data.EffectiveDate.ToString("MM/dd/yyyy") //@@Effective_Date
                                , null //@@Name
                                , data.ErrorCode //@@Error_Code
                                , data.ErrorDescription //@@Error_Description
                                , UserID //@@entryusr
                                , DateTime.Now //@@entrydt
                                , null //@@updateusr
                                , null //@@updatedt
                                , "ACC" //@@Partner_Id
                                , null //@@Numeric_Error_Code_1
                                , null //@@Numeric_Error_Code_2
                                , null //@@Object_No
                                , null //@@Endorsement_Map
                                , null //@@guid
                                , null //@@Error_Description2
                                , null //@@Flag_Value_Chain
                                , null //@@GROUP_NO
                                , 0
                                );
                        #endregion
                    }
                    else if (param == "groupingAR")
                    {
                        #region insert log into acc_grouping_ar
                        #region query
                        string query = @"
                                DECLARE @@no_kontrak VARCHAR(50) = @0
                                        ,@@policy_no VARCHAR(50) = @1
                                        ,@@group_no VARCHAR(50) = @2
                                        ,@@approval_date VARCHAR(50) = @3
                                        ,@@type_insu VARCHAR(50) = @4
                                        ,@@endorsement_no VARCHAR(50) = @5
                                        ,@@ar_status VARCHAR(50) = @6
                                        ,@@send_status VARCHAR(50) = @7
                                        ,@@send_date VARCHAR(50) = @8
                                        ,@@error_code VARCHAR(50) = @9
                                        ,@@error_description VARCHAR(250) = @10
                                        ,@@reff_no VARCHAR(50) = @11
                                        ,@@file_name VARCHAR(50) = @12
                                        ,@@create_by VARCHAR(50) = @13
                                        ,@@create_date DATETIME = @14

                                INSERT INTO [dbo].[acc_grouping_ar]
                                           ([no_kontrak]
                                           ,[policy_no]
                                           ,[group_no]
                                           ,[approval_date]
                                           ,[type_insu]
                                           ,[endorsement_no]
                                           ,[ar_status]
                                           ,[send_status]
                                           ,[send_date]
                                           ,[error_code]
                                           ,[error_description]
                                           ,[reff_no]
                                           ,[file_name]
                                           ,[create_by]
                                           ,[create_date])
                                     VALUES
                                           (@@no_kontrak
                                           ,@@policy_no
                                           ,@@group_no
                                           ,@@approval_date
                                           ,@@type_insu
                                           ,@@endorsement_no
                                           ,@@ar_status
                                           ,@@send_status
                                           ,@@send_date
                                           ,@@error_code
                                           ,@@error_description
                                           ,@@reff_no
                                           ,@@file_name
                                           ,@@create_by
                                           ,@@create_date)";
                        #endregion

                        foreach (var item in data)
                        {
                            db.Execute(query, item.NO_KONTRAK, item.POLICY_NO, item.GROUP_NO, item.APPROVAL_DATE.ToString("dd/MM/yyyy"), item.TYPE_INSU, "0", "1", "0", "", item.ERROR_CODE, item.ERROR_DESCRIPTION, reffNo, FileName,
                                UserID, DateTime.Now);
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }

            return result;
        }


        public ActionResult InsertLogError(string param, string reffNo, string ErrorCode, string ErrorDesc, string FileName = "")
        {
            ActionResult result = new ActionResult();

            string UserID = ConfigurationManager.AppSettings["EntryUserID"];

            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
                {

                    if (param == "newOrder")
                    {
                        #region insert log into acc_new_order
                        #region query
                        string query = @";
                            DECLARE @@error_code VARCHAR(50) = @0
                                ,@@error_description VARCHAR(250) = @1
                                ,@@reff_no VARCHAR(50) = @2
                                ,@@file_name VARCHAR(50) = @3
                                ,@@create_by VARCHAR(50) = @4
                                ,@@create_date DATETIME = @5

                            INSERT INTO [dbo].[acc_new_order]
                                       ([error_code]
                                       ,[error_description]
                                       ,[reff_no]
                                       ,[file_name]
                                       ,[create_by]
                                       ,[create_date])
                                 VALUES
                                      (@@error_code
                                       ,@@error_description
                                       ,@@reff_no
                                       ,@@file_name
                                       ,@@create_by
                                       ,@@create_date)";
                        #endregion
                        db.Execute(query, ErrorCode
                                       , ErrorDesc
                                       , reffNo
                                       , FileName
                                       , UserID
                                       , DateTime.Now);
                        #endregion


                    }
                    else if (param == "cancelOrder")
                    {
                        #region insert log into acc_cancel_order
                        #region query
                        string query = @";
                            DECLARE @@error_code VARCHAR(50) = @0,
                                    @@error_description VARCHAR(250) = @1,
                                    @@reff_no VARCHAR(50) = @2,
                                    @@file_name VARCHAR(50) = @3,
                                    @@create_by VARCHAR(50) = @4,
                                    @@create_date DATETIME = @5

                            INSERT INTO [dbo].[acc_cancel_order]
                                        ([error_code]
                                        ,[error_description]
                                        ,[reff_no]
                                        ,[file_name]
                                        ,[create_by]
                                        ,[create_date])
                                VALUES 
                                        (@@error_code
                                        ,@@error_description
                                        ,@@reff_no
                                        ,@@file_name
                                        ,@@create_by
                                        ,@@create_date)";
                        #endregion

                        db.Execute(query, ErrorCode, ErrorDesc, reffNo, FileName, UserID, DateTime.Now);
                        #endregion
                    }
                    else if (param == "groupingAR")
                    {
                        #region insert log into acc_grouping_ar
                        #region query
                        string query = @"
                                DECLARE @@error_code VARCHAR(50) = @0
                                        ,@@error_description VARCHAR(250) = @1
                                        ,@@reff_no VARCHAR(50) = @2
                                        ,@@file_name VARCHAR(50) = @3
                                        ,@@create_by VARCHAR(50) = @4
                                        ,@@create_date DATETIME = @5

                                INSERT INTO [dbo].[acc_grouping_ar]
                                           ([error_code]
                                           ,[error_description]
                                           ,[reff_no]
                                           ,[file_name]
                                           ,[create_by]
                                           ,[create_date])
                                     VALUES
                                           (@@error_code
                                           ,@@error_description
                                           ,@@reff_no
                                           ,@@file_name
                                           ,@@create_by
                                           ,@@create_date)";
                        #endregion
                        db.Execute(query, ErrorCode, ErrorDesc, reffNo, FileName, UserID, DateTime.Now);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.Message;
                _log.Error(MethodBase.GetCurrentMethod().Name + "Error : " + ex.Message);
            }

            return result;
        }

        private void UpdateB2BRequestStatus(string B2BToken, string reffNo, int enumRequestStatus, string B2BApplicationID, string B2BRequestID)
        {
            //string UpdateB2BRequestStatusUrl = ConfigurationManager.AppSettings["UpdateB2BRequestStatusUrl"];

            //object obj = new
            //{
            //    EnumRequestStatus = enumRequestStatus,
            //    B2BApplicationID = B2BApplicationID,
            //    RefferenceNo = reffNo,
            //    B2BRequestID = B2BRequestID
            //};

            //string json = JsonConvert.SerializeObject(obj);
            //JObject jobject = JObject.Parse(json);

            //var httpResponse = APIGetCaller(UpdateB2BRequestStatusUrl, jobject, B2BToken);


            B2BAPI.UpdateRequestStatus(B2BApplicationID, reffNo, B2BRequestID.ToString(), enumRequestStatus.ToString(), B2BToken);
        }

        public string ParseRequestData(string jsonRequestData)
        {
            string result = "";
            var requestDataParse = JsonConvert.DeserializeObject<dynamic>(jsonRequestData);
            result = requestDataParse.RequestData.ToString();

            return result;
        }
        private string ParseFIleName(string jsonRequestData)
        {
            string result = "";
            var requestDataParse = JsonConvert.DeserializeObject<dynamic>(jsonRequestData);
            if (requestDataParse.FileName != null)
            {
                result = requestDataParse.FileName.ToString();
            }

            return result;
        }
        public bool InsertB2BACC_Logs(B2BACC_Logs data)
        {
            bool result = false;
            #region query
            string query = @" INSERT INTO [dbo].[B2BACC_Logs] ([TrxType] ,[JsonRequest] ,[JsonResponse] ,[OrderNo] ,[PolicyNo] ,[EndorsementNo]  ,[remarks],[Entryusr] ,[Entrydt],[updateusr],[updatedt])
            VALUES (@0 ,@1 ,@2 ,@3
            ,@4 ,@5 ,@6 ,@7 ,@8,@9,@10) ";
            #endregion
            int iRowAffected = 0;

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                iRowAffected = db.Execute(query, data.TrxType, data.JsonRequest, data.JsonResponse, data.OrderNo, data.PolicyNo, data.EndorsementNo, data.Remarks, "b2bsa", data.Entrydt, "b2bsa", data.Updatedt);
            }

            return result;
        }
        public class APIParam
        {
            public string RefferenceNo { get; set; }
            public string B2BToken { get; set; }
        }

        #endregion
    }
}